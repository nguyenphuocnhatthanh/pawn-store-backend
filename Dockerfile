FROM node:12

RUN apt-get update -y && apt-get upgrade -y && apt-get install libhdf5-dev -y
RUN mkdir -p /usr/src/pawn-app && chown -R node:node /usr/src/pawn-app

WORKDIR /usr/src/pawn-app

COPY package.json yarn.lock ./

USER node

RUN yarn config set hdf5_home_linux /usr/lib/x86_64-linux-gnu/hdf5/serial
RUN yarn install --pure-lockfile && yarn global add multi-file-swagger
#RUN npm install "git+https://git@github.com/HDF-NI/hdf5.node.git#v12" --hdf5_home_linux=/usr/lib/x86_64-linux-gnu/hdf5/serial

COPY --chown=node:node . .

EXPOSE 3000

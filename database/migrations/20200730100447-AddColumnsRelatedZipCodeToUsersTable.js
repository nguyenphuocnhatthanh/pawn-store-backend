module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return Promise.all([
      queryInterface.addColumn('users', 'birthday', {
        type: Sequelize.DATE,
      }),
      queryInterface.addColumn('users', 'province', {
        type: Sequelize.STRING,
      }),
      queryInterface.addColumn('users', 'city', {
        type: Sequelize.STRING,
      }),
      queryInterface.addColumn('users', 'district', {
        type: Sequelize.STRING,
      }),
      queryInterface.addColumn('users', 'area', {
        type: Sequelize.STRING,
      }),
      queryInterface.addColumn('users', 'building', {
        type: Sequelize.STRING,
      }),
    ]);
  },

  down: (queryInterface) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    return Promise.all([
      queryInterface.removeColumn('users', 'birthday'),
      queryInterface.removeColumn('users', 'province'),
      queryInterface.removeColumn('users', 'city'),
      queryInterface.removeColumn('users', 'district'),
      queryInterface.removeColumn('users', 'area'),
      queryInterface.removeColumn('users', 'building'),
    ]);
  },
};

const uuid = require('uuid');

module.exports = {
  up: (queryInterface) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
    return queryInterface.bulkInsert(
      'blacklist_domain_emails',
      [
        {
          id: uuid.v4(),
          service_name: 'au',
          domain: 'ezweb.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'au',
          domain: 'biz.ezweb.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'au',
          domain: 'augps.ezweb.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'au',
          domain: 'ido.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'au',
          domain: 'uqmobile.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'au',
          domain: 'au.com',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'au',
          domain: 'biz.au.com',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'NTTドコモ',
          domain: 'docomo.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'NTTドコモ',
          domain: 'mopera.net',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'ソフトバンク',
          domain: 'disney.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'ソフトバンク',
          domain: 'disneymobile.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'ソフトバンク',
          domain: 'i.softbank.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'ソフトバンク',
          domain: 'softbank.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'ソフトバンク',
          domain: 'vodafone.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'PHS・旧ウィルコム」以外',
          domain: 'emnet.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'PHS・旧ウィルコム」以外',
          domain: 'emobile.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'PHS・旧ウィルコム」以外',
          domain: 'emobile-s.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'PHS・旧ウィルコム」以外',
          domain: 'ymobile1.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'PHS・旧ウィルコム」以外',
          domain: 'ymobile.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'PHS・旧ウィルコム」以外',
          domain: 'yahoo.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'PHS・旧ウィルコム',
          domain: 'pdx.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'PHS・旧ウィルコム',
          domain: 'willcom.com',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'PHS・旧ウィルコム',
          domain: 'wcm.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: 'PHS・旧ウィルコム',
          domain: 'y-mobile.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: '旧Jフォン',
          domain: 'jp-c.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: '旧Jフォン',
          domain: 'jp-d.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: '旧Jフォン',
          domain: 'jp-h.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: '旧Jフォン',
          domain: 'jp-k.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: '旧Jフォン',
          domain: 'jp-n.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: '旧Jフォン',
          domain: 'jp-q.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: '旧Jフォン',
          domain: 'jp-r.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: '旧Jフォン',
          domain: 'jp-s.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: '旧Jフォン',
          domain: 'jp-t.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: '旧ツー カー',
          domain: 'sky.tkc.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: '旧ツー カー',
          domain: 'sky.tkk.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
        {
          id: uuid.v4(),
          service_name: '旧ツー カー',
          domain: 'sky.tu-ka.ne.jp',
          created_at: new Date(),
          updated_at: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
    return queryInterface.bulkDelete('blacklist_domain_emails', null, {});
  },
};

class EnsureSensitiveResource {
  /**
   * EnsureSensitiveCollection constructor.
   * @param {Array} protectedFields
   */
  constructor(protectedFields = []) {
    this._protectedFields = protectedFields;
  }

  /**
   * Apply ensure sensitive data paginate
   * @param object
   * @returns {*}
   */
  paginate(object) {
    // eslint-disable-next-line no-param-reassign
    object.data = object.data.map((item) => {
      this._protectedFields.forEach((field) => {
        // eslint-disable-next-line no-param-reassign
        delete item[field];
      });
      return item;
    });

    return object;
  }
}

module.exports = EnsureSensitiveResource;

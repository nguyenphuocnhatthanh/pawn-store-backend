const { convertCamelObjectToSnake } = require('./common');

/**
 * Class to paginate sequelite results.
 */
class SequelizePaginate {
  /** @typedef {import('sequelize').Model} Model */
  /**
   * Method to append paginate method to Model.
   *
   * @param {Model} Model - Sequelize Model.
   * @returns {*} -
   * @example
   * const sequelizePaginate = require('sequelize-paginate')
   *
   * sequelizePaginate.paginate(MyModel)
   */
  // eslint-disable-next-line class-methods-use-this
  paginate(Model) {
    /**
     * @typedef {Object} Paginate Sequelize query options
     * @property {number} [paginate=25] Results per page
     * @property {number} [page=1] Number of page
     */
    /**
     * @typedef {import('sequelize').FindOptions & Paginate} paginateOptions
     */
    /**
     * The paginate result
     * @typedef {Object} PaginateResult
     * @property {Array} docs Docs
     * @property {number} pages Number of page
     * @property {number} total Total of docs
     */
    /**
     * Pagination.
     *
     * @param page
     * @param limit
     * @param {paginateOptions} [params] - Options to filter query.
     * @returns {Promise<{total: *, data: *, lastPage: number, currentPage: number}>} Total pages and docs.
     * @example
     * const { docs, pages, total } = await MyModel.paginate({ page: 1, paginate: 25 })
     * @memberof Model
     */
    const pagination = async function ({ page = 1, limit = 25, ...params } = {}) {
      const options = { ...params };
      const countOptions = Object.keys(options).reduce((acc, key) => {
        if (!['order', 'attributes', 'include'].includes(key)) {
          // eslint-disable-next-line security/detect-object-injection
          acc[key] = options[key];
        }
        return acc;
      }, {});

      let total = await this.count(countOptions);

      if (options.group !== undefined) {
        // @ts-ignore
        total = total.length;
      }

      const lastPage = Math.ceil(total / limit);
      const currentPage = Number(page) > 0 ? page : 1;
      options.limit = limit;
      options.offset = limit * (currentPage - 1);
      /* eslint-disable no-console */
      if (params.limit) {
        console.warn(`(sequelize-pagination) Warning: limit option is ignored.`);
      }
      if (params.offset) {
        console.warn(`(sequelize-pagination) Warning: offset option is ignored.`);
      }
      /* eslint-enable no-console */
      if (params.order) options.order = params.order;
      const data = await this.findAll({ ...options, raw: true, nested: true });
      return convertCamelObjectToSnake({
        data,
        metaData: {
          perPage: limit,
          currentPage,
          lastPage,
          total,
        },
      });
    };
    const instanceOrModel = Model.Instance || Model;
    instanceOrModel.paginate = pagination;
  }
}

module.exports = new SequelizePaginate();

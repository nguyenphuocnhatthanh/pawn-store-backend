const kue = require('kue');
const config = require('../config/config');

const queue = kue.createQueue(config.queue);
queue.setMaxListeners(1000);

module.exports = queue;

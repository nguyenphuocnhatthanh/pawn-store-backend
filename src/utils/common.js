const bcrypt = require('bcryptjs');
const { snakeCase, camelCase, forOwn, isPlainObject, mapKeys } = require('lodash');
const authHeader = require('passport-jwt/lib/auth_header');
const { authenticatePrefix } = require('../services/redis.service');

const AUTH_HEADER = 'authorization';

const camelToSnake = (string) => {
  return string.replace(/[\w]([A-Z])/g, (m) => `${m[0]}_${m[1]}`).toLowerCase();
};

/**
 * @description walk tree object
 * @param {Object | Array} obj
 * @param {Function} cb - callback
 * @returns {Object | Array}
 */
const walk = (obj, cb) => {
  const x = Array.isArray(obj) ? [] : {};

  forOwn(obj, (v, k) => {
    if (isPlainObject(v) || Array.isArray(v)) {
      // eslint-disable-next-line no-param-reassign
      v = walk(v, cb);
    }

    x[cb(k)] = v;
  });

  return x;
};

const convertCamelObjectToSnake = (obj) => {
  return walk(obj, (k) => snakeCase(k));
};

const convertSnakeObjectToCamel = (obj) => {
  return mapKeys(obj, (value, key) => camelCase(key));
};

const getAuthPrefixRedis = (userId) => {
  return `${authenticatePrefix}:${userId}`;
};

const getBearerTokenHeader = (req) => {
  try {
    return authHeader.parse(req.headers[AUTH_HEADER]).value;
  } catch (e) {
    return null;
  }
};

const makePasswordHash = (password) => {
  const salt = bcrypt.genSaltSync();
  return bcrypt.hashSync(password, salt);
};

function chunkArray(arr, chunkSize) {
  const result = [];

  for (let i = 0, len = arr.length; i < len; i += chunkSize) {
    result.push(arr.slice(i, i + chunkSize));
  }

  return result;
}

module.exports = {
  camelToSnake,
  convertCamelObjectToSnake,
  convertSnakeObjectToCamel,
  getAuthPrefixRedis,
  getBearerTokenHeader,
  makePasswordHash,
  chunkArray,
};

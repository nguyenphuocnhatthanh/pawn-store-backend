const redis = require('redis');
const config = require('../config/config');
const logger = require('../config/logger');

const configRedis = config.queue.redis;

const client = redis.createClient({ ...configRedis });

client.on('connect', function () {
  logger.info('Connected to Redis Server');
});
client.on('error', function (err) {
  logger.info(`Connect failed to Redis Server ${err}`);
});

module.exports = client;

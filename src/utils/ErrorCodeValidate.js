const ApiError = require('./ApiError');
const { localeVN } = require('../resources/locales');
/**
 * Handle api error via joi validate
 * @param {string} errorKey
 * @param statusCode
 * @param {string} messageDefault
 * @returns {ApiError}
 * @private
 */
class ErrorCodeValidate {
  constructor(errorKey, statusCode = 500, messageDefault = '', locale = 'vi') {
    this.errorKey = errorKey;
    this.statusCode = statusCode;
    this.messageDefault = messageDefault;
    this.locale = locale;
  }

  setLocale(locale) {
    this.locale = locale;

    return this;
  }

  _loadJsonMessage() {
    return this.locale === 'vi' ? localeVN.validate[this.errorKey] : null;
  }

  parse() {
    const jsonMessage = this._loadJsonMessage();
    const message = jsonMessage ? jsonMessage.message : this.messageDefault;
    this.apiError = new ApiError(this.statusCode, message);

    if (jsonMessage) {
      this.apiError.setErrorCode(jsonMessage.error_code);
    }

    return this.apiError;
  }
}

module.exports = ErrorCodeValidate;

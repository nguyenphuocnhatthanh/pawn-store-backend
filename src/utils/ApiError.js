class ApiError extends Error {
  constructor(statusCode, message, isOperational = true, stack = '', errorCode = 0) {
    super(message);
    this.statusCode = statusCode;
    this.isOperational = isOperational;
    this.errorCode = errorCode;
    if (stack) {
      this.stack = stack;
    } else {
      Error.captureStackTrace(this, this.constructor);
    }
  }

  setErrorCode(errorCode) {
    this.errorCode = errorCode;
  }
}

module.exports = ApiError;

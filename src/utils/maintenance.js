const express = require('express');
const config = require('../config/config');
const catchAsync = require('./catchAsync');

function maintenance(app, options) {
  let mode = false;
  let endpoint = false;
  let url = '/maintenance';
  let status = 503;

  let accessKey;

  if (typeof options === 'boolean') {
    mode = options;
  } else if (typeof options === 'object') {
    mode = options.current || mode;
    endpoint = options.httpEndpoint || endpoint;
    url = options.url || url;
    accessKey = options.accessKey;
    status = options.status || status;
  } else {
    throw new Error('unsuported options');
  }

  const checkAccess = function (req, res, next) {
    if (!accessKey) {
      return next();
    }

    const match = req.query.access_key === accessKey;
    if (match) {
      return next();
    }

    res.sendStatus(401);
  };

  const server = function (appExpress) {
    if (endpoint) {
      const router = express.Router();
      router.post(url, checkAccess, function (req, res) {
        mode = true;
        res.sendStatus(200);
      });

      router.delete(url, checkAccess, function (req, res) {
        mode = false;
        res.sendStatus(200);
      });

      appExpress.use('/', router);
    }
  };

  const handle = catchAsync(async function (req, res) {
    res.status(status);

    const maintenanceTime = config.maintenance_time;

    if (maintenanceTime) {
      const [startTime, endTime] = maintenanceTime.split('~');
      res.json({ start_time: new Date(startTime), end_time: new Date(endTime), current_time: new Date() });
      return;
    }

    return res.json({ start_time: null, end_time: null, current_time: new Date() });
  });

  const middleware = function (req, res, next) {
    if (mode) {
      return handle(req, res);
    }

    next();
  };

  const inject = function (appExpress) {
    appExpress.use(middleware);
    return appExpress;
  };

  return server(inject(app));
}

module.exports = maintenance;

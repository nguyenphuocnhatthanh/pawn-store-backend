const { Sequelize, Model } = require('sequelize');
const database = require('../config/database');
const SequelizePaginate = require('../utils/SequelizePaginate');

class BaseModel extends Model {
  static async updateOrCreate(condition, newItem) {
    const [model, created] = await this.findOrCreate({ where: condition, defaults: newItem });

    if (!created) {
      const instance = await model.update(newItem);

      return instance;
    }

    return model;
  }
}

BaseModel.init(
  {
    id: {
      type: Sequelize.UUID,
      primaryKey: true,
    },
    createdAt: {
      type: Sequelize.DATE,
    },
    updatedAt: {
      type: Sequelize.DATE,
    },
  },
  { underscored: true, sequelize: database.sequelize() }
);

SequelizePaginate.paginate(BaseModel);

module.exports = BaseModel;

const { Sequelize, Model } = require('sequelize');
const uuid = require('uuid');
const database = require('../config/database');
const { tokenType } = require('../enums/token');
const UserModel = require('./user.model');

const tableName = 'tokens';

class Token extends Model {}

Token.init(
  {
    id: {
      type: Sequelize.UUID,
      primaryKey: true,
    },
    token: {
      type: Sequelize.STRING,
    },
    userId: {
      type: Sequelize.UUID,
    },
    type: {
      type: Sequelize.ENUM,
      values: Object.values(tokenType),
    },
    expiredAt: {
      type: Sequelize.DATE,
    },
    createdAt: {
      type: Sequelize.DATE,
    },
    updatedAt: {
      type: Sequelize.DATE,
    },
  },
  { underscored: true, sequelize: database.sequelize(), modelName: 'Token', tableName }
);

Token.beforeCreate((token) => {
  // eslint-disable-next-line no-param-reassign
  token.id = uuid.v4();
});
Token.belongsTo(UserModel, { as: 'user' });

module.exports = Token;

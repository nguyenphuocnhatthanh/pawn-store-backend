const bcrypt = require('bcryptjs');
const { Sequelize } = require('sequelize');
const uuid = require('uuid');
const moment = require('moment');
const database = require('../config/database');
const BaseModel = require('./base.model');

const status = ['Inactive', 'Active', 'Locked'];

class User extends BaseModel {
  isPasswordMatch(password) {
    return bcrypt.compareSync(password, this.password);
  }

  toJSON() {
    const result = super.toJSON();
    delete result.password;
    result.group_name = '株式会社レベルファイブ';
    result.user_type = ' 正常';
    result.expired_date = moment('2020-12-31', 'YYYY-MM-DD');

    return result;
  }

  isInActive() {
    return this.status === 'Inactive';
  }

  isActive() {
    return this.status === 'Active';
  }
}

User.init(
  {
    email: {
      type: Sequelize.STRING,
      unique: true,
    },
    password: {
      type: Sequelize.STRING,
    },
    displayName: {
      type: Sequelize.STRING,
    },
    status: {
      type: Sequelize.ENUM(status),
      defaultValue: 'Inactive',
    },
    province: {
      type: Sequelize.STRING,
    },
    city: {
      type: Sequelize.STRING,
    },
    district: {
      type: Sequelize.STRING,
    },
    area: {
      type: Sequelize.STRING,
    },
    building: {
      type: Sequelize.STRING,
    },
    trialCode: {
      type: Sequelize.STRING,
      unique: true,
    },
    birthday: {
      type: Sequelize.DATE,
    },
    lastLoginAt: {
      type: Sequelize.DATE,
    },
  },
  { underscored: true, sequelize: database.sequelize(), modelName: 'User', tableName: 'users' }
);
User.findByEmail = (email) => {
  return User.findOne({
    where: {
      email: email.toLowerCase(),
    },
    limit: 1,
  });
};

User.beforeCreate((user) => {
  const salt = bcrypt.genSaltSync();
  // eslint-disable-next-line no-param-reassign
  user.id = uuid.v4();
  // eslint-disable-next-line no-param-reassign
  user.password = bcrypt.hashSync(user.password, salt);
});

module.exports = User;

const dotenv = require('dotenv');
const path = require('path');
const Joi = require('@hapi/joi');

dotenv.config({ path: path.join(__dirname, '../../.env') });

const envVarsSchema = Joi.object()
  .keys({
    DB_NAME: Joi.string().required().description('Database name'),
    DB_USERNAME: Joi.string().required().description('Database username'),
    DB_PASSWORD: Joi.string().description('Database password'),
    DB_PORT: Joi.number().description('Database port'),
    DB_TEST_NAME: Joi.string().description('Database name'),
    DB_TEST_USERNAME: Joi.string().description('Database username'),
    DB_TEST_PASSWORD: Joi.string().description('Database password'),
    DB_TEST_PORT: Joi.number().description('Database port'),
  })
  .unknown();

const { value: envVars, error } = envVarsSchema.prefs({ errors: { label: 'key' } }).validate(process.env);

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

module.exports = {
  development: {
    username: envVars.DB_USERNAME,
    password: envVars.DB_PASSWORD,
    database: envVars.DB_NAME,
    port: envVars.DB_PORT,
    host: envVars.DB_HOST || 'localhost',
    dialect: 'mysql',
  },
  testing: {
    username: envVars.DB_TEST_USERNAME,
    password: envVars.DB_TEST_PASSWORD,
    database: envVars.DB_TEST_NAME,
    port: envVars.DB_TEST_PORT,
    host: envVars.DB_TEST_HOST || 'localhost',
    dialect: 'mysql',
  },
  production: {
    username: envVars.DB_USERNAME,
    password: envVars.DB_PASSWORD,
    database: envVars.DB_NAME,
    port: envVars.DB_PORT,
    host: envVars.DB_HOST || 'localhost',
    dialect: 'mysql',
  },
};

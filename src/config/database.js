const Sequelize = require('sequelize');
const config = require('./databaseConfig');

const env = process.env.NODE_ENV || 'development';
const configDatabase = config[env];

class Database {
  constructor() {
    if (env === 'testing') {
      this._sequelize = new Sequelize(configDatabase.database, configDatabase.username, configDatabase.password, {
        host: configDatabase.host,
        dialect: 'mysql',
        pool: {
          max: 5,
          min: 0,
          idle: 10000,
        },
      });
    } else {
      this._sequelize = new Sequelize(configDatabase.database, null, null, {
        dialect: 'mysql',
        replication: {
          read: [
            {
              host: process.env.DB_SLAVE_HOST,
              username: process.env.DB_SLAVE_USERNAME,
              password: process.env.DB_SLAVE_PASSWORD,
            },
          ],
          write: {
            host: process.env.DB_MASTER_HOST,
            username: process.env.DB_MASTER_USERNAME,
            password: process.env.DB_MASTER_PASSWORD,
          },
        },
        pool: {
          max: 5,
          min: 0,
          idle: 10000,
        },
      });
    }
  }

  connect() {
    return this._sequelize
      .authenticate()
      .then(() => {
        // eslint-disable-next-line no-console
        console.log('Connection Database has been established successfully.');
      })
      .catch((err) => {
        // eslint-disable-next-line no-console
        console.error('Unable Database to connect to the database:', err);
      });
  }

  sequelize() {
    return this._sequelize;
  }
}

const database = new Database();

module.exports = database;

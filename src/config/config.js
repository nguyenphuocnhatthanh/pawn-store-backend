const dotenv = require('dotenv');
const path = require('path');
const Joi = require('@hapi/joi');

dotenv.config({ path: path.join(__dirname, '../../.env') });

const envVarsSchema = Joi.object()
  .keys({
    // NODE_ENV: Joi.string().valid('production', 'development', 'test').required(),
    PORT: Joi.number().default(3000),
    JWT_SECRET: Joi.string().required().description('JWT secret key'),
    JWT_ACCESS_EXPIRATION_MINUTES: Joi.number().default(1440).description('minutes after which access tokens expire'),
    JWT_FORGOT_EXPIRATION_MINUTES: Joi.number().default(1440).description('minutes after which forgot tokens expire'),
    JWT_REFRESH_EXPIRATION_DAYS: Joi.number().default(30).description('days after which refresh tokens expire'),
    JWT_ACTIVATE_EXPIRATION_MINUTES: Joi.number().default(43200).description('days after which verify email tokens expire'),
    SMTP_HOST: Joi.string().description('server that will send the emails'),
    SMTP_PORT: Joi.number().description('port to connect to the email server'),
    SMTP_USERNAME: Joi.string().description('username for email server'),
    SMTP_PASSWORD: Joi.string().description('password for email server'),
    SMTP_SECURE: Joi.boolean().default(false).description('secure for email server'),
    EMAIL_FROM: Joi.string().description('the from field in the emails sent by the app'),
    REDIS_HOST: Joi.string().description('server that will queue'),
    REDIS_PORT: Joi.string().description('port to connect queue'),
    REDIS_DB: Joi.string().description('database queue'),
    MAINTENANCE: Joi.boolean().description('Maintenance mode'),
    MAINTENANCE_TIME: Joi.string().allow(null).allow('').optional().description('Maintenance time'),
  })
  .unknown();

const { value: envVars, error } = envVarsSchema.prefs({ errors: { label: 'key' } }).validate(process.env);

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}

module.exports = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  jwt: {
    secret: envVars.JWT_SECRET,
    accessExpirationMinutes: envVars.JWT_ACCESS_EXPIRATION_MINUTES,
    refreshExpirationDays: envVars.JWT_REFRESH_EXPIRATION_DAYS,
    resetPasswordExpirationMinutes: envVars.JWT_FORGOT_EXPIRATION_MINUTES,
    activateUserExpirationMinutes: envVars.JWT_ACTIVATE_EXPIRATION_MINUTES,
  },
  email: {
    smtp: {
      host: envVars.SMTP_HOST,
      port: envVars.SMTP_PORT,
      auth: {
        user: envVars.SMTP_USERNAME,
        pass: envVars.SMTP_PASSWORD,
      },
      secure: envVars.SMTP_SECURE,
      debug: true,
    },
    from: envVars.EMAIL_FROM,
  },
  queue: {
    prefix: 'pawn_store',
    redis: {
      host: envVars.REDIS_HOST,
      port: envVars.REDIS_PORT,
      db: envVars.REDIS_DB,
    },
  },
  maintenance: envVars.MAINTENANCE,
  maintenance_time: envVars.MAINTENANCE_TIME,
};

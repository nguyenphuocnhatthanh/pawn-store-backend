const moment = require('moment');

const objectId = (value, helpers) => {
  if (!value.match(/^[0-9a-fA-F]{24}$/)) {
    return helpers.message('"{{#label}}" must be a valid mongo id');
  }
  return value;
};
// eslint-disable-next-line no-useless-escape
const emailRFC = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{1,}))$/;
// eslint-disable-next-line no-useless-escape
const passwordRFC = /^[a-zA-Z0-9\`~!@#$%^&*()-=[\]_+{}:"'|;,.\/<>?\\s]*$/;

const password = (value, helpers) => {
  if (value.length < 6) {
    return helpers.message('password must be at least 6 characters');
  }
  // if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
  // eslint-disable-next-line no-useless-escape
  if (!value.match(passwordRFC)) {
    return helpers.message('password is invalid');
  }
  return value;
};

const email = (value, helpers) => {
  if (!value.match(emailRFC)) {
    return helpers.message('password is invalid');
  }

  return value;
};

const birthday = (value, helpers) => {
  const date = moment(value, 'YYYY/MM/DD', true);
  if (!date.isValid()) {
    return helpers.message('この日付は無効です。再度、入力をお願いします。');
  }

  if (
    date.isSameOrAfter(moment().add(1, 'days').format('YYYY/MM/DD')) ||
    date.isBefore(moment('1900/01/01', 'YYYY/MM/DD'))
  ) {
    return helpers.message('生年月日は1900/01/01から現在の日付までである必要があります。');
  }
  return value;
};

module.exports = {
  objectId,
  password,
  email,
  birthday,
};

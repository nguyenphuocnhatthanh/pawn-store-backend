const httpStatus = require('http-status');
const { pick } = require('lodash');
const { convertCamelObjectToSnake } = require('../../../utils/common');
const ApiError = require('../../../utils/ApiError');
const catchAsync = require('../../../utils/catchAsync');
const { userService } = require('../../../services');
const EnsureSensitiveResource = require('../../../utils/EnsureSensitiveResource');

const createUser = catchAsync(async (req, res) => {
  const user = await userService.create(req.body);
  res.status(httpStatus.CREATED).send(user);
});

const getUsers = catchAsync(async (req, res) => {
  const options = pick(req.query, ['limit', 'page']);
  const result = await userService.getList(options);
  const resource = new EnsureSensitiveResource(['password']);

  res.send(resource.paginate(result));
});

const getUser = catchAsync(async (req, res) => {
  const user = await userService.show(req.params.userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  res.send(convertCamelObjectToSnake(user.toJSON()));
});

const updateUser = catchAsync(async (req, res) => {
  const user = await userService.update(req.params.userId, req.body);
  res.send(convertCamelObjectToSnake(user.toJSON()));
});

const deleteUser = catchAsync(async (req, res) => {
  await userService.destroy(req.params.userId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createUser,
  getUsers,
  getUser,
  updateUser,
  deleteUser,
};

const express = require('express');
const { auth, validate } = require('../../../middlewares');
const userValidation = require('../validations/user.validation');
const { userController } = require('../controllers');

const router = express.Router();

router
  .route('/')
  .post(auth(), validate(userValidation.createUser), userController.createUser)
  .get(auth(), validate(userValidation.getUsers), userController.getUsers);

router
  .route('/:userId')
  .get(auth(), userController.getUser)
  .patch(auth(), validate(userValidation.updateUser), userController.updateUser)
  .delete(auth(), userController.deleteUser);

module.exports = router;

const Joi = require('@hapi/joi');
const { password, email, birthday } = require('../../../validation/common.validation');

const register = {
  body: Joi.object().keys({
    email: Joi.string().required().max(127).custom(email),
    password: Joi.string().required().max(127).custom(password),
    display_name: Joi.string().max(255).allow('').allow(null).optional(),
    province: Joi.string().required().max(255),
    city: Joi.string().required().max(255),
    district: Joi.string().required().max(255),
    area: Joi.string().allow('').allow(null).max(255).optional(),
    building: Joi.string().required().max(255),
    birthday: Joi.string().required().custom(birthday),
  }),
};

const login = {
  body: Joi.object().keys({
    email: Joi.string().required().max(127).custom(email),
    password: Joi.string().required().max(127).custom(password),
    trial_code: Joi.string().allow('').allow(null).optional(),
  }),
};

const refreshTokens = {
  body: Joi.object().keys({
    refreshToken: Joi.string().required(),
  }),
};

const forgotPassword = {
  body: Joi.object().keys({
    email: Joi.string().required().custom(email),
  }),
};

const resetPassword = {
  query: Joi.object().keys({
    token: Joi.string().required(),
  }),
  body: Joi.object().keys({
    password: Joi.string().required().custom(password),
  }),
};

const activateUser = {
  query: Joi.object().keys({
    token: Joi.string().required(),
  }),
};

const resendEmail = {
  query: Joi.object().keys({
    user_id: Joi.string().required(),
  }),
};

const verifyResetPasswordToken = {
  query: Joi.object().keys({
    token: Joi.string().required().messages({
      'string.empty': 'Token is not allowed to be empty',
    }),
  }),
};

module.exports = {
  register,
  login,
  refreshTokens,
  forgotPassword,
  resetPassword,
  activateUser,
  resendEmail,
  verifyResetPasswordToken,
};

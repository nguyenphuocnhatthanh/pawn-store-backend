const httpStatus = require('http-status');
const catchAsync = require('../../../utils/catchAsync');
const ApiError = require('../../../utils/ApiError');
const { authService, userService, tokenService, redisService } = require('../../../services');
const resetPasswordEmail = require('../../../emails/resetPassword.email');
const activateUserEmail = require('../../../emails/activateUser.email');
const { convertCamelObjectToSnake, getAuthPrefixRedis, getBearerTokenHeader } = require('../../../utils/common');

const register = catchAsync(async (req, res) => {
  const user = await userService.create({ ...req.body, email: req.body.email.toLowerCase() });
  const activateUserToken = await tokenService.generateActivateUserToken(user);
  await activateUserEmail(user.email, user.displayName, activateUserToken);
  return res.status(httpStatus.CREATED).send({ user: convertCamelObjectToSnake(user.toJSON()) });
});

const login = catchAsync(async (req, res) => {
  const { email, password } = req.body;
  const user = await authService.loginUserWithEmailAndPassword(email, password);

  const tokens = await tokenService.generateAuthTokens(user);
  const accessToken = tokens.access.token;

  await redisService.hset(getAuthPrefixRedis(user.id), 'current_access_token', accessToken);
  await redisService.hset(getAuthPrefixRedis(user.id), accessToken, 1);

  await userService.update(user.id, { lastLoginAt: new Date() });

  res.send({ user: convertCamelObjectToSnake(user.toJSON()), tokens });
});

const logout = catchAsync(async (req, res) => {
  const userId = req.user.id;
  const token = getBearerTokenHeader(req);

  redisService.hdel(getAuthPrefixRedis(userId), token);
  res.status(httpStatus.NO_CONTENT).send();
});

const refreshTokens = catchAsync(async (req, res) => {
  const tokens = await authService.refreshAuth(req.body.refreshToken);
  res.send({ ...tokens });
});

const forgotPassword = catchAsync(async (req, res) => {
  const resetPassword = await tokenService.generateResetPasswordToken(req.body.email);
  if (resetPassword) {
    const { user, resetPasswordToken } = resetPassword;
    await resetPasswordEmail(user.email, user.displayName, resetPasswordToken);
  }
  res.status(httpStatus.NO_CONTENT).send();
});

const verifyResetPasswordToken = catchAsync(async (req, res) => {
  try {
    await tokenService.verifyToken(req.query.token, 'resetPassword');
    res.status(httpStatus.NO_CONTENT).send();
  } catch (e) {
    throw new ApiError(httpStatus.BAD_REQUEST, e.message);
  }
});

const resetPassword = catchAsync(async (req, res) => {
  await authService.resetPassword(req.query.token, req.body.password);
  res.status(httpStatus.NO_CONTENT).send();
});

const resendEmail = catchAsync(async (req, res) => {
  const user = await userService.show(req.query.user_id);

  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND);
  }
  if (user.isActive()) {
    throw new ApiError(httpStatus.BAD_REQUEST);
  }

  const activateUserToken = await tokenService.generateActivateUserToken(user);
  await activateUserEmail(user.email, user.displayName, activateUserToken);

  res.status(httpStatus.NO_CONTENT).send();
});

const activateUser = catchAsync(async (req, res) => {
  const user = await authService.activateUser(req.query.token);
  const tokens = await tokenService.generateAuthTokens(user);
  const accessToken = tokens.access.token;
  await redisService.hset(getAuthPrefixRedis(user.id), accessToken, 1);
  await redisService.hset(getAuthPrefixRedis(user.id), 'current_access_token', accessToken);

  res.send({ user: convertCamelObjectToSnake(user.toJSON()), tokens });
});

module.exports = {
  register,
  login,
  logout,
  refreshTokens,
  forgotPassword,
  verifyResetPasswordToken,
  resetPassword,
  resendEmail,
  activateUser,
};

const express = require('express');
const { validate, auth } = require('../../../middlewares');
const { authValidation } = require('../validations');
const { authController } = require('../controllers');

const router = express.Router();

router.post('/register', validate(authValidation.register), authController.register);
router.post('/resend-email', validate(authValidation.resendEmail), authController.resendEmail);
router.post('/activate-user', validate(authValidation.activateUser), authController.activateUser);
router.post('/login', validate(authValidation.login), authController.login);
router.post('/refresh-tokens', validate(authValidation.refreshTokens), authController.refreshTokens);
router.get(
  '/verify-reset-password-token',
  validate(authValidation.verifyResetPasswordToken),
  authController.verifyResetPasswordToken
);
router.post('/forgot-password', validate(authValidation.forgotPassword), authController.forgotPassword);
router.post('/reset-password', validate(authValidation.resetPassword), authController.resetPassword);
router.delete('/logout', auth(), authController.logout);

module.exports = router;

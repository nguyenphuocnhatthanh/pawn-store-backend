const catchAsync = require('../../../utils/catchAsync');
const config = require('../../../config/config');

const index = catchAsync(async (req, res) => {
  const maintenanceTime = config.maintenance_time;

  if (maintenanceTime) {
    const [startTime, endTime] = maintenanceTime.split('~');
    res.send({ start_time: new Date(startTime), end_time: new Date(endTime), current_time: new Date() });
    return;
  }

  res.send({ start_time: null, end_time: null, current_time: new Date() });
});

module.exports = {
  index,
};

const express = require('express');
const { maintenanceController } = require('../controllers');

const router = express.Router();

router.get('/', maintenanceController.index);

module.exports = router;

const Joi = require('@hapi/joi');
const { password, birthday } = require('../../../validation/common.validation');

const update = {
  body: Joi.object().keys({
    password: Joi.string().optional().max(127).custom(password).allow('', null),
    display_name: Joi.string().max(255).allow('', null).optional(),
    province: Joi.string().required().max(255),
    city: Joi.string().required().max(255),
    district: Joi.string().required().max(255),
    area: Joi.string().allow('').allow(null).max(255).optional(),
    building: Joi.string().required().max(255),
    birthday: Joi.string().required().custom(birthday),
  }),
};

module.exports = {
  update,
};

const express = require('express');
const { auth, validate } = require('../../../middlewares');
const { meValidation } = require('../validations');
const { meController } = require('../controllers');

const router = express.Router();

router.get('/', auth(), meController.index);
router.put('/', auth(), validate(meValidation.update), meController.update);

module.exports = router;

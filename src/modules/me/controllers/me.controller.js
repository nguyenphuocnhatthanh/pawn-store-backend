const catchAsync = require('../../../utils/catchAsync');
const { userService } = require('../../../services');
const { convertCamelObjectToSnake } = require('../../../utils/common');

const index = catchAsync(async (req, res) => {
  const user = await userService.show(req.user.id);

  res.send(convertCamelObjectToSnake(user.toJSON()));
});

const update = catchAsync(async (req, res) => {
  const user = await userService.update(req.user.id, {
    ...req.body,
    displayName: req.body.display_name,
  });

  res.send(convertCamelObjectToSnake(user.toJSON()));
});

module.exports = {
  index,
  update,
};

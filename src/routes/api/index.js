const express = require('express');
const authRoute = require('../../modules/auth/routes/route');
const userRoute = require('../../modules/user/routes/route');
const meRoute = require('../../modules/me/routes/route');
const maintenanceRoute = require('../../modules/maintenance/routes/route');

const router = express.Router();

router.use('/auth', authRoute);
router.use('/users', userRoute);
router.use('/me', meRoute);
router.use('/maintenance-mode', maintenanceRoute);

module.exports = router;

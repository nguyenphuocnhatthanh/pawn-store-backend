const schedule = require('node-schedule');
const queue = require('../utils/queue');
const { emailService } = require('../services');

schedule.scheduleJob('* * * * * *', () => {
  queue.process('reset password email', function (data, done) {
    const { to, subject, text } = data.data;
    emailService.sendEmail(to, subject, text);
    // eslint-disable-next-line no-console
    console.log('reset password email success.');
    done();
  });

  queue.process('activate user email', function (data, done) {
    const { to, subject, text } = data.data;
    emailService.sendEmail(to, subject, text);
    // eslint-disable-next-line no-console
    console.log('activate user email success.');
    done();
  });
});

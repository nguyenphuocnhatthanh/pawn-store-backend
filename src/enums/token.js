module.exports.tokenType = {
  reset_password: 'resetPassword',
  refresh_token: 'refreshToken',
  activate_user: 'activateUser',
};

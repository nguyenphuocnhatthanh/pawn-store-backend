const queue = require('../utils/queue');
const config = require('../config/config');

/**
 * Send reset password email
 * @param {string} to
 * @param {string} displayName
 * @param {string} token
 * @returns {Promise}
 */
const sendResetPasswordEmail = async (to, displayName, token) => {
  const subject = 'Reset Password email subject';
  // replace this url with the link to the reset password page of your front-end app
  const text = `<p>Reset Password Email/p>`;

  queue.create('reset password email', { to, subject, text }).removeOnComplete(true).attempts(3).save();
};

module.exports = sendResetPasswordEmail;

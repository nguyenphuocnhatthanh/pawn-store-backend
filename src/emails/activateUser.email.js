const queue = require('../utils/queue');
const config = require('../config/config');

/**
 * Send activate user email
 * @param {string} to
 * @param {string} displayName
 * @param {string} token
 * @returns {Promise}
 */
const sendActivateUserEmail = async (to, displayName, token) => {
  const subject = 'Activate Subject';
  const text = `<p>Activate Email</p>`;
  queue.create('activate user email', { to, subject, text }).removeOnComplete(true).attempts(3).save();
};

module.exports = sendActivateUserEmail;

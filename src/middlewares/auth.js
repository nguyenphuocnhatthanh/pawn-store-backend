const passport = require('passport');
const httpStatus = require('http-status');
const jwt = require('jsonwebtoken');
const ApiError = require('../utils/ApiError');
const { roleRights } = require('../config/roles');
const { redisService } = require('../services');
const { getAuthPrefixRedis, getBearerTokenHeader } = require('../utils/common');

const verifyCallback = (req, resolve, reject, requiredRights) => async (err, user, info) => {
  const token = getBearerTokenHeader(req);

  if (err || info || !user) {
    const decoded = jwt.decode(getBearerTokenHeader(req));

    if (decoded) {
      redisService.hdel(getAuthPrefixRedis(decoded.sub), token);
    }

    return reject(new ApiError(httpStatus.UNAUTHORIZED));
  }

  req.user = user;

  if (user.isInActive()) {
    return reject(
      new ApiError(
        httpStatus.UNAUTHORIZED,
        'あなたのアカウントは有効になっておりません。<br>あなたのメールアドレス宛の認証メールをご確認ください。'
      )
    );
  }

  const isTokenValid = await redisService.hexistsAsync(getAuthPrefixRedis(user.id), token);
  const currentAccessToken = await redisService.hget(getAuthPrefixRedis(user.id), 'current_access_token');

  if (!isTokenValid) {
    return reject(new ApiError(httpStatus.UNAUTHORIZED, 'トークンが無効です'));
  }

  if (token !== currentAccessToken) {
    return reject(
      new ApiError(httpStatus.UNAUTHORIZED, '別のブラウザーにログインしたため、アカウントは自動的にログアウトされました。')
    );
  }

  if (requiredRights.length) {
    const userRights = roleRights.get(user.role);
    const hasRequiredRights = requiredRights.every((requiredRight) => userRights.includes(requiredRight));
    if (!hasRequiredRights && req.params.userId !== user.id) {
      return reject(new ApiError(httpStatus.FORBIDDEN, 'Forbidden'));
    }
  }

  resolve();
};

const auth = (...requiredRights) => async (req, res, next) => {
  return new Promise((resolve, reject) => {
    passport.authenticate('jwt', { session: false }, verifyCallback(req, resolve, reject, requiredRights))(req, res, next);
  })
    .then(() => next())
    .catch((err) => next(err));
};

module.exports = auth;

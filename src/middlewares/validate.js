const Joi = require('@hapi/joi');
const httpStatus = require('http-status');
const { pick } = require('lodash');
const ErrorCodeValidate = require('../utils/ErrorCodeValidate');

const validate = (schema) => (req, res, next) => {
  const validSchema = pick(schema, ['params', 'query', 'body']);
  const object = pick(req, Object.keys(validSchema));
  const { value, error } = Joi.compile(validSchema)
    .prefs({ errors: { label: 'key' } })
    .validate(object);

  if (error) {
    const {
      type,
      context: { key },
    } = error.details[0];

    const errorKey = `${key}.${type}`;
    const message = error.details.map((details) => details.message).join(', ');
    const errorCode = new ErrorCodeValidate(errorKey, httpStatus.UNPROCESSABLE_ENTITY, message);
    const apiError = errorCode.parse();

    return next(apiError);
  }
  Object.assign(req, value);
  return next();
};

module.exports = validate;

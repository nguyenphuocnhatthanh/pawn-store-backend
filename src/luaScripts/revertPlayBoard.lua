local user_id = tostring(KEYS[1])
local user_bet = "user_"..user_id.."_bet"
local user_deal = "user_"..user_id.."_dealer"
local user_result = "user_"..user_id.."_result"
local data = redis.call("mget", user_bet, user_deal, user_result)

if data[1] ~= nil then
  redis.call("set", user_bet, data[1]:sub(0,data[1]:len()-1))
end

if data[2] ~= nil then
  redis.call("set", user_deal, data[2]:sub(0,data[2]:len()-1))
end

if data[3] ~= nil then
  redis.call("set", user_result, data[3]:sub(0,data[3]:len()-1))
end

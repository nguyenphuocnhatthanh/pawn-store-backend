#!/usr/bin/env node
const program = require('commander');
require('../schedules/forceInsertUserHistory');
require('../schedules/removeExpiredSessionToken');

program.version('0.0.1');
program.description('Baccarat command line');

program
  .command('schedule:run')
  .description('Run schedule')
  .action(function () {
    // eslint-disable-next-line no-console
    console.log('Schedule is running!!!');
  });

// allow commander to parse `process.argv`
program.parse(process.argv);

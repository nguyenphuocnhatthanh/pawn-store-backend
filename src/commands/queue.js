#!/usr/bin/env node
const program = require('commander');
require('../schedules/queue');

program.version('0.0.1');
program.description('Baccarat command line');

program
  .command('queue:work')
  .description('Listen queue worker')
  .action(function () {
    // eslint-disable-next-line no-console
    console.log('Listen queue work');
  });

// allow commander to parse `process.argv`
program.parse(process.argv);

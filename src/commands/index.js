#!/usr/bin/env node
const program = require('commander');
const fs = require('fs');
const path = require('path');

const consolePath = path.join(__dirname, 'console');

program.version('0.0.1');
program.description('Baccarat command line');

fs.readdirSync(consolePath).forEach(function (file) {
  // eslint-disable-next-line import/no-dynamic-require,global-require
  require(`./console/${file}`);
});

// allow commander to parse `process.argv`
program.parse(process.argv);

const program = require('commander');
const uuid = require('uuid');
const bcrypt = require('bcryptjs');

const { User } = require('../../models');

program
  .command('generate:users <number>')
  .description('Generate users')
  .action(async function (number) {
    // eslint-disable-next-line no-console
    console.info('Processing generate users', number);
    const salt = bcrypt.genSaltSync();
    const password = bcrypt.hashSync('12345678a', salt);

    const data = [];
    for (let i = 1; i <= number; i += 1) {
      // eslint-disable-next-line no-await-in-loop
      data.push({
        id: uuid.v4(),
        email: `user_test${i}@yopmail.com`,
        password,
        created_at: new Date(),
        updated_at: new Date(),
      });
    }

    await User.bulkCreate(data);
    // eslint-disable-next-line no-console
    console.info('Generated users');
  });

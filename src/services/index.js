module.exports.authService = require('./Auth');
module.exports.userService = require('./User');
module.exports.tokenService = require('./token.service');
module.exports.emailService = require('./email.service');
module.exports.redisService = require('./redis.service');

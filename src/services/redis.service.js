const { promisify } = require('util');
const client = require('../utils/redis');

const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const delAsync = promisify(client.del).bind(client);
const hexistsAsync = promisify(client.hexists).bind(client);
const hset = promisify(client.hset).bind(client);
const hget = promisify(client.hget).bind(client);
const hdel = promisify(client.hdel).bind(client);
const hkeys = promisify(client.hkeys).bind(client);
const del = promisify(client.del).bind(client);
const set = promisify(client.set).bind(client);
const get = promisify(client.get).bind(client);
// eslint-disable-next-line security/detect-non-literal-fs-filename
const exists = promisify(client.exists).bind(client);
const append = promisify(client.append).bind(client);
const keys = promisify(client.keys).bind(client);
const mget = promisify(client.mget).bind(client);
const setnx = promisify(client.setnx).bind(client);
const lpush = promisify(client.lpush).bind(client);
const lrange = promisify(client.lrange).bind(client);

const authenticatePrefix = 'auth';

module.exports = {
  getAsync,
  setAsync,
  delAsync,
  hexistsAsync,
  hset,
  hget,
  hdel,
  hkeys,
  authenticatePrefix,
  del,
  set,
  get,
  exists,
  append,
  keys,
  mget,
  setnx,
  lpush,
  lrange,
};

module.exports.getList = require('./getList.service');
module.exports.show = require('./show.service');
module.exports.create = require('./create.service');
module.exports.update = require('./update.service');
module.exports.destroy = require('./destroy.service');

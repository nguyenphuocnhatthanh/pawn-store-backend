const { User } = require('../../models');

/**
 * Get user by id
 * @param {string} id
 * @returns {Promise<User>}
 */
const show = async (id) => {
  return User.findByPk(id);
};

module.exports = show;

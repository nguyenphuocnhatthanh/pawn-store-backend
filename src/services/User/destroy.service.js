const httpStatus = require('http-status');
const show = require('./show.service');
const ApiError = require('../../utils/ApiError');

/**
 * Delete user by id
 * @param {ObjectId} userId
 * @returns {Promise<User>}
 */
const destroy = async (userId) => {
  const user = await show(userId);
  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'User not found');
  }
  await user.destroy();
  return user;
};

module.exports = destroy;

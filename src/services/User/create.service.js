const httpStatus = require('http-status');
const { User } = require('../../models');
const ApiError = require('../../utils/ApiError');

/**
 * Create a user
 * @param {Object} userBody
 * @returns {Promise<User>}
 */
const create = async (userBody) => {
  if (await User.findByEmail(userBody.email)) {
    throw new ApiError(httpStatus.BAD_REQUEST, 'あなたのメールアドレスは既に登録済みです。');
  }
  const user = await User.create({ ...userBody, displayName: userBody.display_name, trialCode: userBody.trial_code });
  return user;
};

module.exports = create;

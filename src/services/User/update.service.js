const httpStatus = require('http-status');
const ApiError = require('../../utils/ApiError');
const show = require('./show.service');
const { makePasswordHash } = require('../../utils/common');

/**
 * Update user by id
 * @param {string} userId
 * @param {Object} updateBody
 * @returns {Promise<User>}
 */
const update = async (userId, updateBody) => {
  const user = await show(userId);
  const data = updateBody;

  if (!user) {
    throw new ApiError(httpStatus.NOT_FOUND, 'あなたのアカウントは見つかりませんでした。');
  }

  if (data.password === undefined || data.password === '' || data.password === null) {
    delete data.password;
  } else {
    data.password = makePasswordHash(data.password);
  }

  Object.assign(user, data);
  await user.save();
  return user;
};

module.exports = update;

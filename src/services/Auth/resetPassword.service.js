const httpStatus = require('http-status');
const tokenService = require('../token.service');
const userService = require('../User');
const Token = require('../../models/token.model');
const ApiError = require('../../utils/ApiError');

/**
 * Reset password
 * @param {string} resetPasswordToken
 * @param {string} newPassword
 * @returns {Promise}
 */
const resetPassword = async (resetPasswordToken, newPassword) => {
  try {
    const resetPasswordTokenDoc = await tokenService.verifyToken(resetPasswordToken, 'resetPassword');

    const user = await userService.show(resetPasswordTokenDoc.userId);
    if (!user) {
      throw new Error('User not found');
    }
    await Token.destroy({
      where: { user_id: user.id, type: 'resetPassword' },
    });
    await userService.update(user.id, { password: newPassword });
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, error.message);
  }
};

module.exports = resetPassword;

module.exports.loginUserWithEmailAndPassword = require('./login.service');
module.exports.resetPassword = require('./resetPassword.service');
module.exports.refreshAuth = require('./refreshAuth.service');
module.exports.activateUser = require('./activateUser.service');

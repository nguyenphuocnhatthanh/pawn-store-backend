const httpStatus = require('http-status');
const tokenService = require('../token.service');
const userService = require('../User');
const Token = require('../../models/token.model');
const ApiError = require('../../utils/ApiError');
const { tokenType } = require('../../enums/token');

/**
 * activate user
 * @param {string} activateUserToken
 * @returns {Promise}
 */
const activateUser = async (activateUserToken) => {
  try {
    const activateUserTokenDoc = await tokenService.verifyToken(activateUserToken, tokenType.activate_user);
    const user = await userService.show(activateUserTokenDoc.userId);
    if (!user) {
      throw new Error('User not found');
    }
    await Token.destroy({
      where: { user_id: user.id, type: tokenType.activate_user },
    });
    // Change user->status to 'Active'
    await userService.update(user.id, { status: 'Active' });
    return user;
  } catch (error) {
    throw new ApiError(httpStatus.BAD_REQUEST, error.message);
  }
};

module.exports = activateUser;

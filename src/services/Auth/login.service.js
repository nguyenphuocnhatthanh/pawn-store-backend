const httpStatus = require('http-status');
const User = require('../../models/user.model');
const ApiError = require('../../utils/ApiError');

/**
 * Login with username and password
 * @param {string} email
 * @param {string} password
 * @returns {Promise<User>}
 */
const loginUserWithEmailAndPassword = async (email, password) => {
  const user = await User.findByEmail(email);
  if (!user || !(await user.isPasswordMatch(password))) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect email or password');
  }

  if (user.isInActive()) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'User is inactivate');
  }

  return user;
};

module.exports = loginUserWithEmailAndPassword;

const jwt = require('jsonwebtoken');
const moment = require('moment');
// const httpStatus = require('http-status');
const config = require('../config/config');
const User = require('../models/user.model');
const { Token } = require('../models');
// const ApiError = require('../utils/ApiError');
const { tokenType } = require('../enums/token');

/**
 * Generate token
 * @param {ObjectId} userId
 * @param {Moment} expires
 * @param {string} [secret]
 * @returns {string}
 */
const generateToken = (userId, expires, secret = config.jwt.secret) => {
  const payload = {
    sub: userId,
    iat: moment().unix(),
    exp: expires.unix(),
  };
  return jwt.sign(payload, secret);
};

/**
 * Save a token
 * @param {string} token
 * @param {string} userId
 * @param {Moment} expires
 * @param {string} type
 * @returns {Promise<Token>}
 */
const saveToken = async (token, userId, expires, type) => {
  const tokenDoc = await Token.create({
    token,
    userId,
    expiredAt: expires.toDate(),
    type,
  });
  return tokenDoc;
};

/**
 * Verify token and return token doc (or throw an error if it is not valid)
 * @param {string} token
 * @param {string} type
 * @returns {Promise<Token>}
 */
const verifyToken = async (token, type) => {
  try {
    const payload = jwt.verify(token, config.jwt.secret);

    const tokenDoc = await Token.findOne({
      where: {
        token,
        type,
        user_id: payload.sub,
        // blacklisted: false,
      },
    });

    if (!tokenDoc) {
      throw new Error('Token not found');
    }
    return tokenDoc;
  } catch (e) {
    throw new Error(e.getMessage());
  }
};

/**
 * Generate auth tokens
 * @param {User} user
 * @returns {Promise<Object>}
 */
const generateAuthTokens = async (user) => {
  const accessTokenExpires = moment().add(config.jwt.accessExpirationMinutes, 'minutes');
  const accessToken = generateToken(user.id, accessTokenExpires);

  // const refreshTokenExpires = moment().add(config.jwt.refreshExpirationDays, 'days');
  // const refreshToken = generateToken(user.id, refreshTokenExpires);
  // await saveToken(refreshToken, user.id, refreshTokenExpires, 'refresh');

  return {
    access: {
      token: accessToken,
      expired_at: accessTokenExpires.toDate(),
    },
    // refresh: {
    //   token: refreshToken,
    //   expired_at: refreshTokenExpires.toDate(),
    // },
  };
};

/**
 * Generate reset password token
 * @param {string} email
 * @returns {Promise<string>|Boolean}
 */
const generateResetPasswordToken = async (email) => {
  const user = await User.findByEmail(email);
  if (!user) {
    return false;
    // throw new ApiError(httpStatus.UNPROCESSABLE_ENTITY, 'No users found with this email');
  }
  const expires = moment().add(config.jwt.resetPasswordExpirationMinutes, 'minutes');
  const resetPasswordToken = generateToken(user.id, expires);
  await saveToken(resetPasswordToken, user.id, expires, tokenType.reset_password);
  return { user, resetPasswordToken };
};

/**
 * Generate activate user token
 * @param {string} email
 * @returns {Promise<string>|Boolean}
 */
const generateActivateUserToken = async (user) => {
  if (!user) {
    return false;
    // throw new ApiError(httpStatus.UNPROCESSABLE_ENTITY, 'No users found with this email');
  }
  const expires = moment().add(config.jwt.activateUserExpirationMinutes, 'minutes');
  const activateUserToken = generateToken(user.id, expires);
  await saveToken(activateUserToken, user.id, expires, tokenType.activate_user);
  return activateUserToken;
};

module.exports = {
  generateToken,
  saveToken,
  verifyToken,
  generateAuthTokens,
  generateResetPasswordToken,
  generateActivateUserToken,
};

// const mongoose = require('mongoose');
const database = require('../../src/config/database');

const setupTestDB = () => {
  beforeAll(async () => {
    // await mongoose.connect(config.mongoose.url, config.mongoose.options);
    Object.values(database.sequelize().models).map((model) => {
      if (model.getTableName() !== 'base_models') {
        return model.destroy({ truncate: true });
      }

      return model;
    });
  });

  beforeEach(async () => {
    // await Promise.all(Object.values(mongoose.connection.collections).map(async (collection) => collection.deleteMany()));
  });

  afterAll(async () => {
    // await mongoose.disconnect();
  });
};

module.exports = setupTestDB;

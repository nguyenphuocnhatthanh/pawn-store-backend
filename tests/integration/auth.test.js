const request = require('supertest');
const faker = require('faker');
const httpStatus = require('http-status');
const uuid = require('uuid');
// const httpMocks = require('node-mocks-http');
const moment = require('moment');
const bcrypt = require('bcryptjs');
const app = require('../../src/app');
const config = require('../../src/config/config');
// const auth = require('../../src/middlewares/auth');
const { tokenService, emailService } = require('../../src/services');
// const ApiError = require('../../src/utils/ApiError');
const setupTestDB = require('../utils/setupTestDB');
const { User, Token } = require('../../src/models');
// const { roleRights } = require('../../src/config/roles');
const { userOne, insertUsers } = require('../fixtures/user.fixture');
// const { userOneAccessToken, adminAccessToken } = require('../fixtures/token.fixture');

setupTestDB();

describe('Auth routes', () => {
  describe('POST /api/auth/register', () => {
    let newUser;
    beforeEach(() => {
      newUser = {
        display_name: faker.name.findName(),
        email: faker.internet.email().toLowerCase(),
        zip_code: '1234567',
        province: 'province',
        city: 'city',
        district: 'district',
        area: 'area',
        building: 'building',
        birthday: '2010/01/01',
        password: 'password',
      };
    });

    test('should return 201 and successfully register user if request data is ok', async () => {
      const res = await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.CREATED);

      expect(res.body.user).not.toHaveProperty('password');
      expect(res.body.user).toEqual({
        id: expect.anything(),
        display_name: newUser.display_name,
        email: newUser.email,
        status: 'Inactive',
        zip_code: newUser.zip_code,
        province: newUser.province,
        city: newUser.city,
        district: newUser.district,
        area: newUser.area,
        building: newUser.building,
        birthday: moment(newUser.birthday, 'YYYY/MM/DD').toISOString(),
        created_at: expect.anything(),
        updated_at: expect.anything(),
        group_name: expect.anything(),
        user_type: expect.anything(),
        expired_date: expect.anything(),
      });

      const dbUser = await User.findByPk(res.body.user.id);
      expect(dbUser).toBeDefined();
      expect(dbUser.password).not.toBe(newUser.password);
      // expect(dbUser).toMatchObject({ name: newUser.name, email: newUser.email, role: 'user' });

      // expect(res.body.tokens).toEqual({
      //   access: { token: expect.anything(), expired_at: expect.anything() },
      // });
    });

    test('should return 400 error if email is invalid', async () => {
      newUser.email = 'invalidEmail';

      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);

      newUser.email = `${faker.lorem.words(128)}@gmail.com`;
      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 422 error if password length is less than 6 characters', async () => {
      newUser.password = 'short';

      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 422 error if password length is greater than 127 characters', async () => {
      newUser.password = faker.lorem.words(128);

      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 422 error if zip code is invalid', async () => {
      newUser.zip_code = 'zipcode';
      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);

      newUser.zip_code = '12345678';
      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);

      newUser.zip_code = '123456a';
      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 422 error if zip code is empty', async () => {
      newUser.zip_code = '';

      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 422 error if province is empty', async () => {
      newUser.province = '';

      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 422 error if province is too long', async () => {
      newUser.province = faker.lorem.words(256);

      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 422 error if city is empty', async () => {
      newUser.city = '';

      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 422 error if city is too long', async () => {
      newUser.city = faker.lorem.words(256);

      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 422 error if district is empty', async () => {
      newUser.district = '';

      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 422 error if district is too long', async () => {
      newUser.district = faker.lorem.words(256);

      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 422 error if building is empty', async () => {
      newUser.building = '';

      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 422 error if building is too long', async () => {
      newUser.building = faker.lorem.words(256);

      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 422 error if birthday is empty', async () => {
      newUser.birthday = '';

      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 422 error if birthday is invalid format', async () => {
      newUser.birthday = '2019/02';
      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);

      newUser.birthday = '1899/12/31';
      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);

      newUser.birthday = moment().add(1, 'days').format('YYYY/MM/DD');
      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 400 error if email is already used', async () => {
      const existUser = { ...newUser, id: uuid.v4(), created_at: new Date(), updated_at: new Date() };
      await insertUsers([existUser]);

      await request(app).post('/api/auth/register').send(newUser).expect(httpStatus.BAD_REQUEST);
    });

    // test('should return 400 error if password does not contain both letters and numbers', async () => {
    //   newUser.password = 'password';
    //
    //   await request(app).post('/v1/auth/register').send(newUser).expect(httpStatus.BAD_REQUEST);
    //
    //   newUser.password = '11111111';
    //
    //   await request(app).post('/v1/auth/register').send(newUser).expect(httpStatus.BAD_REQUEST);
    // });
  });

  // describe('POST /v1/auth/login', () => {
  //   test('should return 200 and login user if email and password match', async () => {
  //     await insertUsers([userOne]);
  //     const loginCredentials = {
  //       email: userOne.email,
  //       password: userOne.password,
  //     };
  //
  //     const res = await request(app).post('/v1/auth/login').send(loginCredentials).expect(httpStatus.OK);
  //
  //     expect(res.body.user).toEqual({
  //       id: expect.anything(),
  //       name: userOne.name,
  //       email: userOne.email,
  //       role: userOne.role,
  //     });
  //
  //     expect(res.body.tokens).toEqual({
  //       access: { token: expect.anything(), expires: expect.anything() },
  //       refresh: { token: expect.anything(), expires: expect.anything() },
  //     });
  //   });
  //
  //   test('should return 401 error if there are no users with that email', async () => {
  //     const loginCredentials = {
  //       email: userOne.email,
  //       password: userOne.password,
  //     };
  //
  //     const res = await request(app).post('/v1/auth/login').send(loginCredentials).expect(httpStatus.UNAUTHORIZED);
  //
  //     expect(res.body).toEqual({ code: httpStatus.UNAUTHORIZED, message: 'Incorrect email or password' });
  //   });
  //
  //   test('should return 401 error if password is wrong', async () => {
  //     await insertUsers([userOne]);
  //     const loginCredentials = {
  //       email: userOne.email,
  //       password: 'wrongPassword1',
  //     };
  //
  //     const res = await request(app).post('/v1/auth/login').send(loginCredentials).expect(httpStatus.UNAUTHORIZED);
  //
  //     expect(res.body).toEqual({ code: httpStatus.UNAUTHORIZED, message: 'Incorrect email or password' });
  //   });
  // });
  //
  // describe('POST /v1/auth/refresh-tokens', () => {
  //   test('should return 200 and new auth tokens if refresh token is valid', async () => {
  //     await insertUsers([userOne]);
  //     const expires = moment().add(config.jwt.refreshExpirationDays, 'days');
  //     const refreshToken = tokenService.generateToken(userOne._id, expires);
  //     await tokenService.saveToken(refreshToken, userOne._id, expires, 'refresh');
  //
  //     const res = await request(app).post('/v1/auth/refresh-tokens').send({ refreshToken }).expect(httpStatus.OK);
  //
  //     expect(res.body).toEqual({
  //       access: { token: expect.anything(), expires: expect.anything() },
  //       refresh: { token: expect.anything(), expires: expect.anything() },
  //     });
  //
  //     const dbRefreshTokenDoc = await Token.findOne({ token: res.body.refresh.token });
  //     expect(dbRefreshTokenDoc).toMatchObject({ type: 'refresh', user: userOne._id, blacklisted: false });
  //
  //     const dbRefreshTokenCount = await Token.countDocuments();
  //     expect(dbRefreshTokenCount).toBe(1);
  //   });
  //
  //   test('should return 400 error if refresh token is missing from request body', async () => {
  //     await request(app).post('/v1/auth/refresh-tokens').send().expect(httpStatus.BAD_REQUEST);
  //   });
  //
  //   test('should return 401 error if refresh token is signed using an invalid secret', async () => {
  //     await insertUsers([userOne]);
  //     const expires = moment().add(config.jwt.refreshExpirationDays, 'days');
  //     const refreshToken = tokenService.generateToken(userOne._id, expires, 'invalidSecret');
  //     await tokenService.saveToken(refreshToken, userOne._id, expires, 'refresh');
  //
  //     await request(app).post('/v1/auth/refresh-tokens').send({ refreshToken }).expect(httpStatus.UNAUTHORIZED);
  //   });
  //
  //   test('should return 401 error if refresh token is not found in the database', async () => {
  //     await insertUsers([userOne]);
  //     const expires = moment().add(config.jwt.refreshExpirationDays, 'days');
  //     const refreshToken = tokenService.generateToken(userOne._id, expires);
  //
  //     await request(app).post('/v1/auth/refresh-tokens').send({ refreshToken }).expect(httpStatus.UNAUTHORIZED);
  //   });
  //
  //   test('should return 401 error if refresh token is blacklisted', async () => {
  //     await insertUsers([userOne]);
  //     const expires = moment().add(config.jwt.refreshExpirationDays, 'days');
  //     const refreshToken = tokenService.generateToken(userOne._id, expires);
  //     await tokenService.saveToken(refreshToken, userOne._id, expires, 'refresh', true);
  //
  //     await request(app).post('/v1/auth/refresh-tokens').send({ refreshToken }).expect(httpStatus.UNAUTHORIZED);
  //   });
  //
  //   test('should return 401 error if refresh token is expired', async () => {
  //     await insertUsers([userOne]);
  //     const expires = moment().subtract(1, 'minutes');
  //     const refreshToken = tokenService.generateToken(userOne._id, expires);
  //     await tokenService.saveToken(refreshToken, userOne._id, expires, 'refresh');
  //
  //     await request(app).post('/v1/auth/refresh-tokens').send({ refreshToken }).expect(httpStatus.UNAUTHORIZED);
  //   });
  //
  //   test('should return 401 error if user is not found', async () => {
  //     const expires = moment().add(config.jwt.refreshExpirationDays, 'days');
  //     const refreshToken = tokenService.generateToken(userOne._id, expires);
  //     await tokenService.saveToken(refreshToken, userOne._id, expires, 'refresh');
  //
  //     await request(app).post('/v1/auth/refresh-tokens').send({ refreshToken }).expect(httpStatus.UNAUTHORIZED);
  //   });
  // });

  describe('POST /api/auth/forgot-password', () => {
    beforeEach(() => {
      jest.spyOn(emailService.transport, 'sendMail').mockResolvedValue();
    });

    test('should return 204 and send reset password email to the user', async () => {
      const user = { ...userOne, email: faker.internet.email().toLowerCase() };
      await insertUsers([user]);
      //   await insertUsers([userOne]);
      //   const sendResetPasswordEmailSpy = jest.spyOn(emailService, 'sendResetPasswordEmail');

      await request(app).post('/api/auth/forgot-password').send({ email: user.email }).expect(httpStatus.NO_CONTENT);

      //   expect(sendResetPasswordEmailSpy).toHaveBeenCalledWith(userOne.email, expect.any(String));
      //   const resetPasswordToken = sendResetPasswordEmailSpy.mock.calls[0][1];
      const dbResetPasswordTokenDoc = await Token.findOne({
        where: {
          user_id: user.id,
        },
      });
      expect(dbResetPasswordTokenDoc).toBeDefined();
    });

    // test('should return 422 if email does not belong to any user', async () => {
    //   await request(app)
    //     .post('/api/auth/forgot-password')
    //     .send({ email: userOne.email })
    //     .expect(httpStatus.UNPROCESSABLE_ENTITY);
    // });

    test('should return 422 if email is missing', async () => {
      // await insertUsers([userOne]);

      await request(app).post('/api/auth/forgot-password').send().expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should return 422 if email is invalid', async () => {
      // await insertUsers([userOne]);

      await request(app).post('/api/auth/forgot-password').send({ email: 'email' }).expect(httpStatus.UNPROCESSABLE_ENTITY);
    });
  });

  describe('GET /api/auth/verify-reset-password-token', () => {
    test('should be return 204 if the token is valid', async () => {
      const user = { ...userOne, id: uuid.v4(), email: 'validtoken@yopmail.com' };
      await insertUsers([user]);
      const expires = moment().add(config.jwt.resetPasswordExpirationMinutes, 'minutes');
      const resetPasswordToken = tokenService.generateToken(user.id, expires);
      await tokenService.saveToken(resetPasswordToken, user.id, expires, 'resetPassword');

      await request(app)
        .get('/api/auth/verify-reset-password-token')
        .query({ token: resetPasswordToken })
        .send()
        .expect(httpStatus.NO_CONTENT);
    });

    test('should be return 422 if the token is missing', async () => {
      await request(app).get('/api/auth/verify-reset-password-token').send().expect(httpStatus.UNPROCESSABLE_ENTITY);
    });

    test('should be return 400 if the token is expired time', async () => {
      const user = { ...userOne, id: uuid.v4(), email: 'tokenexpired@yopmail.com' };
      await insertUsers([user]);
      const expires = moment().subtract(1, 'minutes');
      const resetPasswordToken = tokenService.generateToken(user.id, expires);
      await tokenService.saveToken(resetPasswordToken, userOne.id, expires, 'resetPassword');

      await request(app)
        .get('/api/auth/verify-reset-password-token')
        .query({ token: resetPasswordToken })
        .send({ password: 'password2' })
        .expect(httpStatus.BAD_REQUEST);
    });
  });

  describe('POST /api/auth/reset-password', () => {
    test('should return 204 and reset the password', async () => {
      const user = { ...userOne, id: uuid.v4(), email: 'testreset@yopmail.com' };
      await insertUsers([user]);
      const expires = moment().add(config.jwt.resetPasswordExpirationMinutes, 'minutes');
      const resetPasswordToken = tokenService.generateToken(user.id, expires);
      await tokenService.saveToken(resetPasswordToken, user.id, expires, 'resetPassword');

      await request(app)
        .post('/api/auth/reset-password')
        .query({ token: resetPasswordToken })
        .send({ password: 'password2' })
        .expect(httpStatus.NO_CONTENT);

      const dbUser = await User.findByPk(user.id);
      const isPasswordMatch = await bcrypt.compare('password2', dbUser.password);
      expect(isPasswordMatch).toBe(true);

      const dbResetPasswordTokenCount = await Token.count({
        where: {
          user_id: user.id,
          type: 'resetPassword',
        },
      });
      await expect(dbResetPasswordTokenCount).toBe(0);
    });

    test('should return 422 if reset password token is missing', async () => {
      // await insertUsers([userOne]);

      await request(app)
        .post('/api/auth/reset-password')
        .send({ password: 'password2' })
        .expect(httpStatus.UNPROCESSABLE_ENTITY);
    });
    //
    //   test('should return 401 if reset password token is blacklisted', async () => {
    //     await insertUsers([userOne]);
    //     const expires = moment().add(config.jwt.resetPasswordExpirationMinutes, 'minutes');
    //     const resetPasswordToken = tokenService.generateToken(userOne._id, expires);
    //     await tokenService.saveToken(resetPasswordToken, userOne._id, expires, 'resetPassword', true);
    //
    //     await request(app)
    //       .post('/v1/auth/reset-password')
    //       .query({ token: resetPasswordToken })
    //       .send({ password: 'password2' })
    //       .expect(httpStatus.UNAUTHORIZED);
    //   });

    test('should return 400 if reset password token is expired', async () => {
      const user = { ...userOne, id: uuid.v4(), email: 'resetexpired@yopmail.com' };
      await insertUsers([user]);
      const expires = moment().subtract(1, 'minutes');
      const resetPasswordToken = tokenService.generateToken(user.id, expires);
      await tokenService.saveToken(resetPasswordToken, userOne.id, expires, 'resetPassword');

      await request(app)
        .post('/api/auth/reset-password')
        .query({ token: resetPasswordToken })
        .send({ password: 'password2' })
        .expect(httpStatus.BAD_REQUEST);
    });

    test('should return 400 if user is not found', async () => {
      const userIdNotFound = '-1';
      const expires = moment().add(config.jwt.resetPasswordExpirationMinutes, 'minutes');
      const resetPasswordToken = tokenService.generateToken(userIdNotFound, expires);
      await tokenService.saveToken(resetPasswordToken, userIdNotFound, expires, 'resetPassword');

      await request(app)
        .post('/api/auth/reset-password')
        .query({ token: resetPasswordToken })
        .send({ password: 'password2' })
        .expect(httpStatus.BAD_REQUEST);
    });

    test('should return 400 if password is missing or invalid', async () => {
      const user = { ...userOne, id: uuid.v4(), email: 'badpassword@yopmail.com' };
      await insertUsers([user]);
      const expires = moment().add(config.jwt.resetPasswordExpirationMinutes, 'minutes');
      const resetPasswordToken = tokenService.generateToken(user.id, expires);
      await tokenService.saveToken(resetPasswordToken, user.id, expires, 'resetPassword');

      // missing password
      await request(app)
        .post('/api/auth/reset-password')
        .query({ token: resetPasswordToken })
        .expect(httpStatus.UNPROCESSABLE_ENTITY);

      // short password
      await request(app)
        .post('/api/auth/reset-password')
        .query({ token: resetPasswordToken })
        .send({ password: 'short' })
        .expect(httpStatus.UNPROCESSABLE_ENTITY);
    });
  });
});

// describe('Auth middleware', () => {
//   test('should call next with no errors if access token is valid', async () => {
//     await insertUsers([userOne]);
//     const req = httpMocks.createRequest({ headers: { Authorization: `Bearer ${userOneAccessToken}` } });
//     const next = jest.fn();
//
//     await auth()(req, httpMocks.createResponse(), next);
//
//     expect(next).toHaveBeenCalledWith();
//     expect(req.user._id).toEqual(userOne._id);
//   });
//
//   test('should call next with unauthorized error if access token is not found in header', async () => {
//     await insertUsers([userOne]);
//     const req = httpMocks.createRequest();
//     const next = jest.fn();
//
//     await auth()(req, httpMocks.createResponse(), next);
//
//     expect(next).toHaveBeenCalledWith(expect.any(ApiError));
//     expect(next).toHaveBeenCalledWith(
//       expect.objectContaining({ statusCode: httpStatus.UNAUTHORIZED, message: 'Please authenticate' })
//     );
//   });
//
//   test('should call next with unauthorized error if access token is not a valid jwt token', async () => {
//     await insertUsers([userOne]);
//     const req = httpMocks.createRequest({ headers: { Authorization: 'Bearer randomToken' } });
//     const next = jest.fn();
//
//     await auth()(req, httpMocks.createResponse(), next);
//
//     expect(next).toHaveBeenCalledWith(expect.any(ApiError));
//     expect(next).toHaveBeenCalledWith(
//       expect.objectContaining({ statusCode: httpStatus.UNAUTHORIZED, message: 'Please authenticate' })
//     );
//   });
//
//   test('should call next with unauthorized error if access token is generated with an invalid secret', async () => {
//     await insertUsers([userOne]);
//     const tokenExpires = moment().add(config.jwt.accessExpirationMinutes, 'minutes');
//     const accessToken = tokenService.generateToken(userOne._id, tokenExpires, 'invalidSecret');
//     const req = httpMocks.createRequest({ headers: { Authorization: `Bearer ${accessToken}` } });
//     const next = jest.fn();
//
//     await auth()(req, httpMocks.createResponse(), next);
//
//     expect(next).toHaveBeenCalledWith(expect.any(ApiError));
//     expect(next).toHaveBeenCalledWith(
//       expect.objectContaining({ statusCode: httpStatus.UNAUTHORIZED, message: 'Please authenticate' })
//     );
//   });
//
//   test('should call next with unauthorized error if access token is expired', async () => {
//     await insertUsers([userOne]);
//     const tokenExpires = moment().subtract(1, 'minutes');
//     const accessToken = tokenService.generateToken(userOne._id, tokenExpires);
//     const req = httpMocks.createRequest({ headers: { Authorization: `Bearer ${accessToken}` } });
//     const next = jest.fn();
//
//     await auth()(req, httpMocks.createResponse(), next);
//
//     expect(next).toHaveBeenCalledWith(expect.any(ApiError));
//     expect(next).toHaveBeenCalledWith(
//       expect.objectContaining({ statusCode: httpStatus.UNAUTHORIZED, message: 'Please authenticate' })
//     );
//   });
//
//   test('should call next with unauthorized error if user is not found', async () => {
//     const req = httpMocks.createRequest({ headers: { Authorization: `Bearer ${userOneAccessToken}` } });
//     const next = jest.fn();
//
//     await auth()(req, httpMocks.createResponse(), next);
//
//     expect(next).toHaveBeenCalledWith(expect.any(ApiError));
//     expect(next).toHaveBeenCalledWith(
//       expect.objectContaining({ statusCode: httpStatus.UNAUTHORIZED, message: 'Please authenticate' })
//     );
//   });
//
//   test('should call next with forbidden error if user does not have required rights and userId is not in params', async () => {
//     await insertUsers([userOne]);
//     const req = httpMocks.createRequest({ headers: { Authorization: `Bearer ${userOneAccessToken}` } });
//     const next = jest.fn();
//
//     await auth('anyRight')(req, httpMocks.createResponse(), next);
//
//     expect(next).toHaveBeenCalledWith(expect.any(ApiError));
//     expect(next).toHaveBeenCalledWith(expect.objectContaining({ statusCode: httpStatus.FORBIDDEN, message: 'Forbidden' }));
//   });
//
//   test('should call next with no errors if user does not have required rights but userId is in params', async () => {
//     await insertUsers([userOne]);
//     const req = httpMocks.createRequest({
//       headers: { Authorization: `Bearer ${userOneAccessToken}` },
//       params: { userId: userOne._id.toHexString() },
//     });
//     const next = jest.fn();
//
//     await auth('anyRight')(req, httpMocks.createResponse(), next);
//
//     expect(next).toHaveBeenCalledWith();
//   });
//
//   test('should call next with no errors if user has required rights', async () => {
//     await insertUsers([admin]);
//     const req = httpMocks.createRequest({
//       headers: { Authorization: `Bearer ${adminAccessToken}` },
//       params: { userId: userOne._id.toHexString() },
//     });
//     const next = jest.fn();
//
//     await auth(...roleRights.get('admin'))(req, httpMocks.createResponse(), next);
//
//     expect(next).toHaveBeenCalledWith();
//   });
// });

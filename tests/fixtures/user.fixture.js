// const mongoose = require('mongoose');
// const bcrypt = require('bcryptjs');
const uuid = require('uuid');
const faker = require('faker');
const User = require('../../src/models/user.model');

const password = 'password1';
// const salt = bcrypt.genSaltSync(8);
// const hashedPassword = bcrypt.hashSync(password, salt);

const userOne = {
  id: uuid.v4(),
  display_name: faker.name.findName(),
  email: faker.internet.email().toLowerCase(),
  password,
  // role: 'user',
  created_at: new Date(),
  updated_at: new Date(),
};
// const userTwo = {
//   _id: mongoose.Types.ObjectId(),
//   name: faker.name.findName(),
//   email: faker.internet.email().toLowerCase(),
//   password,
//   role: 'user',
// };
//
// const admin = {
//   _id: mongoose.Types.ObjectId(),
//   name: faker.name.findName(),
//   email: faker.internet.email().toLowerCase(),
//   password,
//   role: 'admin',
// }

const insertUsers = async (users) => {
  await User.bulkCreate(users);
};

module.exports = {
  userOne,
  // userTwo,
  // admin,
  insertUsers,
};
